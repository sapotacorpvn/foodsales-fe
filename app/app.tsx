"use client";

import { Inter } from "next/font/google";
import { ThemeProvider } from "next-themes";
import { StoreProvider } from "easy-peasy";

import "./globals.css";
import i18n from "./i18n";
import store from "./store";

const inter = Inter({ subsets: ["latin"] });

export const metadata = {
  title: "Next.js Tutorial",
  description: "A Next.js tutorial using the App Router",
};

export default function App({ children }: { children: React.ReactNode }) {
  return (
    <html
      lang={`${i18n.language}`}
      style={{ colorScheme: "light" }}
      className="light"
    >
      <body className={inter.className}>
        <StoreProvider store={store}>
          <ThemeProvider defaultTheme="light" attribute="class">
            {children}
          </ThemeProvider>
        </StoreProvider>
      </body>
    </html>
  );
}
