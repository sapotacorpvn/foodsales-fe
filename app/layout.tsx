import App from "./app";

export const metadata = {
  title: "Food",
  description: "List Food",
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return <App>{children}</App>;
}
