export interface IGetListFood {
  Pages?: number;
  ItemPerPage?: number;
  SearchTerm?: string;
  OrderedFrom?: any;
  OrderedTo?: any;
  SortByField?: string;
  SortASC: boolean;
}

export interface ICreateFood {
  region?: string;
  city?: string;
  category?: string;
  product?: string;
  quantity?: number;
  unitPrice?: number;
}

export interface IUpdateFood {
  id: string;
  region?: string;
  city?: string;
  category?: string;
  product?: string;
  quantity?: number;
  unitPrice?: number;
}

export interface IDeleteFood {
  id: string;
}
