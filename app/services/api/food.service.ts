import BaseAxios from "@/app/utils/api/baseAxios";

import {
  ICreateFood,
  IDeleteFood,
  IGetListFood,
  IUpdateFood,
} from "../interface/food.interface";

const getListFood = (params: IGetListFood) => {
  return BaseAxios({
    url: `/Orders`,
    method: "GET",
    params,
  });
};

const createFood = (data: ICreateFood) => {
  return BaseAxios({
    url: `/Orders`,
    method: "POST",
    data,
  });
};

const updateFood = (data: IUpdateFood) => {
  return BaseAxios({
    url: `/Orders?id=${data?.id}`,
    method: "PATCH",
    data,
  });
};

const deleteFood = (data: IDeleteFood) => {
  return BaseAxios({
    url: `/Orders?id=${data?.id}`,
    method: "DELETE",
  });
};

export { getListFood, createFood, updateFood, deleteFood };
