import i18n from "@/app/i18n";
import axios from "axios";

const BaseAxios = axios.create({
  baseURL: `${process.env.NEXT_PUBLIC_BASE_URL}`,
  headers: {
    "Content-Type": "application/json",
    Accept: "application/json",
  },
});

BaseAxios.interceptors.request.use(
  async (config) => {
    let token;
    let language;
    try {
      token = sessionStorage.getItem("token") || localStorage.getItem("token");
      language = i18n.language;
    } catch (e) {}
    if (token !== null) {
      config.headers.Authorization = `Bearer ${token}`;
    }
    config.headers["X-localization"] = language || "en";

    return config;
  },
  (error) => {
    Promise.reject(error);
  },
);

// after send request
BaseAxios.interceptors.response.use(
  (response) => {
    return response;
  },
  (error) => {
    return Promise.reject(error);
  },
);

export default BaseAxios;
