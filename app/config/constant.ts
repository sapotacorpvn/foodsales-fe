export const SORT_STATUS = {
  ASC: 1,
  DESC: -1,
  CLEAN: 0,
};
