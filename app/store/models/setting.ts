import { action, Action } from "easy-peasy";

import { TypeStatusNotification } from "@/app/components/notification";

export interface ISettingModel {
  showNotification: boolean;
  setShowNotification: Action<ISettingModel, boolean>;

  statusNotification: keyof typeof TypeStatusNotification;
  setStatusNotification: Action<
    ISettingModel,
    keyof typeof TypeStatusNotification
  >;

  messageNotification: string;
  setMessageNotification: Action<ISettingModel, string>;
}

export const settingModel: ISettingModel = {
  showNotification: false,
  setShowNotification: action((state, payload) => {
    state.showNotification = payload;
  }),

  statusNotification: "success",
  setStatusNotification: action((state, payload) => {
    state.statusNotification = payload;
  }),

  messageNotification: "",
  setMessageNotification: action((state, payload) => {
    state.messageNotification = payload;
  }),
};
