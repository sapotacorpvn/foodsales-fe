import {
  createStore,
  createTypedHooks,
  StateMapper,
  ActionMapper,
} from "easy-peasy";

import { settingModel as setting, ISettingModel } from "./models/setting";

export interface StoreModel {
  setting: ISettingModel;
}

const storeModel = {
  setting,
};

export const { useStoreActions, useStoreState, useStoreDispatch, useStore } =
  createTypedHooks<StoreModel>();

interface IStateMapper extends StateMapper<StoreModel> {}
interface IActionMapper extends ActionMapper<StoreModel, keyof StoreModel> {}

// Setting
export const settingStateSelector = (state: IStateMapper) => state.setting;
export const settingActionSelector = (state: IActionMapper) => state.setting;

const store = createStore(storeModel, {
  name: "store",
  // middleware,
});

export default store;
