"use client";

interface IInput
  extends React.DetailedHTMLProps<
    React.InputHTMLAttributes<HTMLInputElement>,
    HTMLInputElement
  > {
  classNameWrapper?: string;
  classNameLabel?: string;
  classNameInput?: string;
  label?: string;
  messages?: string;
  id?: string;
  success?: boolean;
  error?: boolean;
  isRequired?: boolean;
  icon?: any;
  innerRef?: any;
  setDate?: any;
  unit?: boolean;
}

const Input = ({
  classNameWrapper,
  isRequired,
  classNameLabel,
  classNameInput,
  error,
  innerRef,
  ...props
}: IInput) => {
  const _checkStatus = () => {
    let result =
      "w-full leading-5 relative py-2 px-4 rounded bg-white font-normal placeholder-gray-400 border border-gray-300 focus:outline-none focus:border-gray-400 focus:ring-0 dark:text-gray-300 dark:bg-gray-700 dark:border-gray-700 dark:focus:border-gray-600";
    if (props.disabled) {
      result =
        "bg-gray-100 border py-2 px-4 border-gray-300 text-gray-500 rounded focus:ring-gray-300 focus:border-gray-300 block w-full p-2.5 cursor-not-allowed dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-gray-400 dark:focus:ring-blue-500 dark:focus:border-blue-500 opacity-100";
    } else if (props?.success) {
      result =
        "border py-2 px-4 border-green-500 text-green-900 dark:text-green-400 placeholder-green-700 dark:placeholder-green-500 rounded focus:ring-green-500 focus:border-green-500 block w-full p-2.5 dark:bg-gray-700 dark:border-green-500";
    } else if (error) {
      result =
        "border py-2 px-4 border-red-500 text-red-500 rounded focus:ring-red-500 dark:bg-gray-700 focus:border-red-500 block w-full p-2.5 dark:text-red-500 dark:placeholder-red-500 dark:border-red-500";
    }

    result = result + " text-sm";

    return result;
  };

  const _renderRequired = () => (
    <>{isRequired && <span className="text-red-500">*</span>}</>
  );

  return (
    <div className={`${classNameWrapper}`}>
      {props?.label &&
        (props.unit ? (
          <label
            htmlFor={props?.id}
            className={`block text-blueGray-600 text-sm font-bold mb-2 text-start ${classNameLabel}`}
          >
            {props?.label} (&#13217;) {_renderRequired()}
          </label>
        ) : (
          <label
            htmlFor={props?.id}
            className={`block text-blueGray-600 text-sm font-bold mb-2 text-start ${classNameLabel}`}
          >
            {props?.label} {_renderRequired()}
          </label>
        ))}
      <div className="relative grow">
        <input
          type={props?.type}
          id={props?.id}
          autoComplete="off"
          {...props}
          ref={innerRef}
          placeholder={props?.disabled ? "" : props?.placeholder}
          className={`${_checkStatus()} ${props?.className} ${classNameInput}`}
        />
        {props?.icon}
      </div>

      {props?.success && !!props?.messages && (
        <div className="mt-2 text-sm text-green-600 dark:text-green-500 text-start">
          {props?.messages}
        </div>
      )}
      {error && !!props?.messages && (
        <div className="mt-2 text-sm text-red-500 dark:text-red-500 text-start">
          {props?.messages}
        </div>
      )}
    </div>
  );
};

export default Input;
