"use client";

import { useEffect, useRef, useState } from "react";
import moment from "moment-timezone";

import i18n from "@/app/i18n";
import ModalLayout from "@/app/layouts/modalLayout";
import Input from "../input";
import Button from "../button";
import { createFood, updateFood } from "@/app/services/api/food.service";
import { useStoreActions } from "easy-peasy";
import { settingActionSelector } from "@/app/store";

interface IFood
  extends React.DetailedHTMLProps<
    React.HTMLAttributes<HTMLDivElement>,
    HTMLDivElement
  > {
  open: boolean;
  setOpen: any;
}

export default function Food({ open, setOpen }: IFood) {
  const createOnRef: any = useRef();
  const regionRef: any = useRef();
  const cityRef: any = useRef();
  const categoryRef: any = useRef();
  const productRef: any = useRef();
  const quantityRef: any = useRef();

  const [isLoadingCallApi, setIsLoadingCallApi] = useState(false);
  const [formData, setFormData] = useState({
    id: "",
    createOn: moment().format("YYYY-MM-DD"),
    region: "",
    city: "",
    category: "",
    product: "",
    quantity: "",
    unitPrice: "",
    totalPrice: "",
  });
  const [isCreateOnError, setIsCreateOnError] = useState(false);
  const [messageCreateOnError, setMessageCreateOnError] = useState("");
  const [isRegionError, setIsRegionError] = useState(false);
  const [messageRegionError, setMessageRegionError] = useState("");
  const [isCityError, setIsCityError] = useState(false);
  const [messageCityError, setMessageCityError] = useState("");
  const [isCategoryError, setIsCategoryError] = useState(false);
  const [messageCategoryError, setMessageCategoryError] = useState("");
  const [isProductError, setIsProductError] = useState(false);
  const [messageProductError, setMessageProductError] = useState("");
  const [isQuantityError, setIsQuantityError] = useState(false);
  const [messageQuantityError, setMessageQuantityError] = useState("");
  const [isUnitPriceError, setIsUnitPriceError] = useState(false);
  const [messageUnitPriceError, setMessageUnitPriceError] = useState("");

  const { setShowNotification, setStatusNotification, setMessageNotification } =
    useStoreActions(settingActionSelector);

  useEffect(() => {
    const food = localStorage.getItem("food");

    if (food) {
      setFormData({
        ...formData,
        ...JSON.parse(food),
        createOn: moment(JSON.parse(food)?.createOn).format("YYYY-MM-DD"),
      });
    }

    return () => {
      localStorage.removeItem("food");
    };
  }, []);

  const _onClose = () => {
    setOpen(false);
    setIsCreateOnError(false);
    setIsRegionError(false);
    setIsCityError(false);
    setIsCategoryError(false);
    setIsProductError(false);
    setIsQuantityError(false);
  };

  const _changeValue = (e: any, key: string) => {
    setFormData({
      ...formData,
      [`${key}`]: e?.target?.value,
    });
  };

  const _handleAction = async () => {
    try {
      setIsLoadingCallApi(true);
      if (
        formData?.createOn &&
        formData?.region &&
        formData?.city &&
        formData?.category &&
        formData?.product &&
        formData?.quantity &&
        Number(formData?.quantity) > 0 &&
        formData?.unitPrice &&
        Number(formData?.unitPrice) > 0
      ) {
        if (formData?.id) {
          await updateFood({
            ...formData,
            quantity: Number(formData?.quantity),
            unitPrice: Number(formData?.unitPrice),
          });
          setShowNotification(true);
          setStatusNotification("success");
          setMessageNotification(`${i18n.t("food_sales.edit_food_success")}`);
        } else {
          await createFood({
            ...formData,
            quantity: Number(formData?.quantity),
            unitPrice: Number(formData?.unitPrice),
          });
          setShowNotification(true);
          setStatusNotification("success");
          setMessageNotification(`${i18n.t("food_sales.add_food_success")}`);
        }

        setOpen(false);
        setIsLoadingCallApi(false);
      } else {
        _handleError();
        setIsLoadingCallApi(false);
      }
    } catch (error) {
      setIsLoadingCallApi(false);
      if (formData?.id) {
        setShowNotification(true);
        setStatusNotification("error");
        setMessageNotification(`${i18n.t("food_sales.edit_food_error")}`);
      } else {
        setShowNotification(true);
        setStatusNotification("error");
        setMessageNotification(`${i18n.t("food_sales.add_food_error")}`);
      }
    }
  };

  const _handleError = () => {
    if (!formData?.createOn) {
      createOnRef.current?.focus();
      setIsCreateOnError(true);
      setMessageCreateOnError(`${i18n.t("food_sales.order_date_require")}`);
      return;
    }

    if (!formData?.region) {
      regionRef.current?.focus();
      setIsRegionError(true);
      setMessageRegionError(`${i18n.t("food_sales.region_require")}`);
      return;
    }

    if (!formData?.city) {
      cityRef.current?.focus();
      setIsCityError(true);
      setMessageCityError(`${i18n.t("food_sales.city_require")}`);
      return;
    }

    if (!formData?.category) {
      categoryRef.current?.focus();
      setIsCategoryError(true);
      setMessageCategoryError(`${i18n.t("food_sales.category_require")}`);
      return;
    }

    if (!formData?.product) {
      productRef.current?.focus();
      setIsProductError(true);
      setMessageProductError(`${i18n.t("food_sales.product_require")}`);
      return;
    }

    if (!formData?.quantity) {
      quantityRef.current?.focus();
      setIsQuantityError(true);
      setMessageQuantityError(`${i18n.t("food_sales.quantity_require")}`);
      return;
    }

    if (Number(formData?.quantity) <= 0) {
      quantityRef.current?.focus();
      setIsQuantityError(true);
      setMessageQuantityError(`${i18n.t("food_sales.quantity_error")}`);
      return;
    }

    if (!formData?.unitPrice) {
      quantityRef.current?.focus();
      setIsUnitPriceError(true);
      setMessageUnitPriceError(`${i18n.t("food_sales.unit_price_require")}`);
      return;
    }

    if (Number(formData?.unitPrice) <= 0) {
      quantityRef.current?.focus();
      setIsUnitPriceError(true);
      setMessageUnitPriceError(`${i18n.t("food_sales.unit_price_error")}`);
      return;
    }
  };

  return (
    <ModalLayout open={open} onClose={_onClose} setOpen={setOpen}>
      <div className="max-w-full w-full">
        <div className="relative">
          <div className="p-6">
            <div>
              <h1 className="text-2xl leading-normal font-bold text-gray-800 dark:text-gray-300 text-center">
                {i18n.t("food_sales.title")}
              </h1>
              <hr className="block w-12 h-0.5 mx-auto my-6 mt-0 bg-gray-700 border-gray-700" />
              <div className="grid grid-cols-2 gap-3">
                <Input
                  innerRef={createOnRef}
                  label={`${i18n.t("food_sales.order_date")}`}
                  placeholder={`${i18n.t("food_sales.order_date_placeholder")}`}
                  id={"createOn"}
                  type="date"
                  classNameWrapper="w-full"
                  value={formData.createOn}
                  onChange={(e) => {
                    _changeValue(e, "createOn");
                    setIsCreateOnError(false);
                  }}
                  error={isCreateOnError}
                  messages={messageCreateOnError}
                  isRequired={true}
                  autoFocus
                  disabled
                />
                <Input
                  innerRef={regionRef}
                  label={`${i18n.t("food_sales.region")}`}
                  placeholder={`${i18n.t("food_sales.region_placeholder")}`}
                  id={"region"}
                  classNameWrapper="w-full"
                  value={formData.region}
                  onChange={(e) => {
                    _changeValue(e, "region");
                    setIsRegionError(false);
                  }}
                  error={isRegionError}
                  messages={messageRegionError}
                  isRequired={true}
                />
                <Input
                  innerRef={cityRef}
                  label={`${i18n.t("food_sales.city")}`}
                  placeholder={`${i18n.t("food_sales.city_placeholder")}`}
                  id={"city"}
                  classNameWrapper="w-full"
                  value={formData.city}
                  onChange={(e) => {
                    _changeValue(e, "city");
                    setIsCityError(false);
                  }}
                  error={isCityError}
                  messages={messageCityError}
                  isRequired={true}
                />
                <Input
                  innerRef={categoryRef}
                  label={`${i18n.t("food_sales.category")}`}
                  placeholder={`${i18n.t("food_sales.category_placeholder")}`}
                  id={"category"}
                  classNameWrapper="w-full"
                  value={formData.category}
                  onChange={(e) => {
                    _changeValue(e, "category");
                    setIsCategoryError(false);
                  }}
                  error={isCategoryError}
                  messages={messageCategoryError}
                  isRequired={true}
                />
                <Input
                  innerRef={productRef}
                  label={`${i18n.t("food_sales.product")}`}
                  placeholder={`${i18n.t("food_sales.product_placeholder")}`}
                  id={"product"}
                  classNameWrapper="w-full"
                  value={formData.product}
                  onChange={(e) => {
                    _changeValue(e, "product");
                    setIsProductError(false);
                  }}
                  error={isProductError}
                  messages={messageProductError}
                  isRequired={true}
                />
                <Input
                  innerRef={quantityRef}
                  label={`${i18n.t("food_sales.quantity")}`}
                  placeholder={`${i18n.t("food_sales.quantity_placeholder")}`}
                  id={"quantity"}
                  type="number"
                  classNameWrapper="w-full"
                  value={formData.quantity}
                  onChange={(e) => {
                    _changeValue(e, "quantity");
                    setIsQuantityError(false);
                  }}
                  error={isQuantityError}
                  messages={messageQuantityError}
                  isRequired={true}
                />
                <Input
                  innerRef={quantityRef}
                  label={`${i18n.t("food_sales.unit_price")}`}
                  placeholder={`${i18n.t("food_sales.unit_price_placeholder")}`}
                  id={"unit_price"}
                  type="number"
                  classNameWrapper="w-full"
                  value={formData.unitPrice}
                  onChange={(e) => {
                    _changeValue(e, "unitPrice");
                    setIsUnitPriceError(false);
                  }}
                  error={isUnitPriceError}
                  messages={messageUnitPriceError}
                  isRequired={true}
                />
                <Input
                  innerRef={quantityRef}
                  label={`${i18n.t("food_sales.total_price")}`}
                  placeholder={`${i18n.t(
                    "food_sales.total_price_placeholder",
                  )}`}
                  id={"total_price"}
                  type="number"
                  classNameWrapper="w-full"
                  value={Number(formData.unitPrice) * Number(formData.quantity)}
                  onChange={(e) => {
                    _changeValue(e, "totalPrice");
                  }}
                  isRequired={true}
                  disabled
                />
              </div>
              <div className="grid grid-cols-2">
                <Button
                  typeButton="elevated"
                  className="mt-6 mr-2"
                  loading={false}
                  onClick={_onClose}
                >
                  {i18n.t("food_sales.cancel")}
                </Button>
                <Button
                  typeButton="primary"
                  className="mt-6 ml-2"
                  loading={isLoadingCallApi}
                  disabled={isLoadingCallApi}
                  onClick={_handleAction}
                >
                  {i18n.t("food_sales.save")}
                </Button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </ModalLayout>
  );
}
