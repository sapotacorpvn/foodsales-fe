import { Fragment, useRef } from "react";
import { Dialog, Transition } from "@headlessui/react";
import {
  CheckCircleIcon,
  ExclamationTriangleIcon,
  XMarkIcon,
} from "@heroicons/react/24/outline";
import dynamic from "next/dynamic";

import i18n from "@/app/i18n";

const Button = dynamic(() => import("../button"), { ssr: false });

export enum TypeStatusModal {
  "success",
  "error",
  "warning",
}
interface IModal {
  open: boolean;
  onClose(value: boolean): void;
  setOpen: any;
  title?: string;
  content?: string;
  status?: keyof typeof TypeStatusModal;
  handleOk?: any;
  textOk?: any;
}

export default function Modal({ status = "success", ...props }: IModal) {
  const cancelButtonRef = useRef(null);

  return (
    <Transition.Root show={props?.open} as={Fragment}>
      <Dialog
        as="div"
        className="relative z-[100001]"
        initialFocus={cancelButtonRef}
        onClose={props.onClose}
      >
        <Transition.Child
          as={Fragment}
          enter="ease-out duration-300"
          enterFrom="opacity-0"
          enterTo="opacity-100"
          leave="ease-in duration-200"
          leaveFrom="opacity-100"
          leaveTo="opacity-0"
        >
          <div className="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity" />
        </Transition.Child>

        <div className="fixed inset-0 z-[100002]">
          <div className="flex min-h-full items-center justify-center p-4 text-center">
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
              enterTo="opacity-100 translate-y-0 sm:scale-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100 translate-y-0 sm:scale-100"
              leaveTo="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
            >
              <Dialog.Panel className="relative transform overflow-hidden rounded-lg bg-white text-left shadow-xl transition-all sm:my-8 sm:w-full sm:max-w-fit">
                <div className="flex justify-end pl-4 pr-2">
                  <XMarkIcon
                    onClick={() => props.onClose(false)}
                    className="inline-block w-6 sm:w-8 h-6 sm:h-8 m-1 sm:m-2 cursor-pointer"
                  />
                </div>
                <div className="bg-white px-4 pb-4 sm:p-6 sm:pt-0 sm:pb-4">
                  <div
                    className={`justify-center sm:flex ${
                      !props?.content ? "sm:items-end" : "sm:items-start"
                    }`}
                  >
                    {status == TypeStatusModal[0] && (
                      <div className="mx-auto flex h-12 w-12 flex-shrink-0 items-center justify-center rounded-full bg-green-100 sm:mx-0 sm:h-10 sm:w-10">
                        <CheckCircleIcon
                          className="h-6 w-6 text-green-400"
                          aria-hidden="true"
                        />
                      </div>
                    )}
                    {status == TypeStatusModal[1] && (
                      <div className="mx-auto flex h-12 w-12 flex-shrink-0 items-center justify-center rounded-full bg-red-100 sm:mx-0 sm:h-10 sm:w-10">
                        <ExclamationTriangleIcon
                          className="h-6 w-6 text-red-600"
                          aria-hidden="true"
                        />
                      </div>
                    )}
                    {status == TypeStatusModal[2] && (
                      <div className="mx-auto flex h-12 w-12 flex-shrink-0 items-center justify-center rounded-full bg-yellow-100 sm:mx-0 sm:h-10 sm:w-10">
                        <ExclamationTriangleIcon
                          className="h-6 w-6 text-yellow-600"
                          aria-hidden="true"
                        />
                      </div>
                    )}
                    <div className="mt-3 text-center sm:ml-4 sm:mt-0 h-[2.5rem] flex items-center sm:text-left justify-center">
                      <Dialog.Title
                        as="h3"
                        className="text-base font-semibold leading-6 text-gray-900"
                      >
                        {props?.title}
                      </Dialog.Title>
                      <div className="mt-2">
                        <p className="text-sm text-gray-500">
                          {props?.content}
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="bg-gray-50 pt-3 pb-[2rem] justify-center sm:flex sm:flex-row-reverse px-[5rem]">
                  <Button
                    typeButton="primary"
                    onClick={() => {
                      props?.setOpen(false);
                      props?.handleOk && props?.handleOk();
                    }}
                    className="w-full sm:ml-3 sm:w-auto md:w-[10rem]"
                  >
                    {props.textOk ?? i18n.t("modal.ok")}
                  </Button>
                  <Button
                    typeButton="elevated"
                    onClick={() => {
                      props?.onClose && props?.onClose(false);
                    }}
                    className="mt-3 w-full sm:mt-0 sm:w-auto md:w-[10rem]"
                  >
                    {i18n.t("Cancel")}
                  </Button>
                </div>
              </Dialog.Panel>
            </Transition.Child>
          </div>
        </div>
      </Dialog>
    </Transition.Root>
  );
}
