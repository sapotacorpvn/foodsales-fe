"use client";

import moment from "moment-timezone";

import i18n from "@/app/i18n";
import { SORT_STATUS } from "@/app/config/constant";
import { useCallback } from "react";
import { formatMoneyUSD } from "@/app/utils/functions/formatString";

interface ITable {
  classNameWrapper?: string;
  title?: string;
  textButton?: string;
  dataList?: any[];
  sort?: any;
  setSort?: any;
  setOpen?: any;
  setOpenDelete?: any;
  isLoadingCallApi?: boolean;
  indexItemLast?: number;
}

const Table = ({
  classNameWrapper,
  title,
  textButton,
  dataList = [],
  sort,
  setSort,
  setOpen,
  setOpenDelete,
  isLoadingCallApi,
  indexItemLast,
}: ITable) => {
  const _handleSort = (value: string) => {
    setSort({
      createdon: SORT_STATUS.CLEAN,
      region: SORT_STATUS.CLEAN,
      city: SORT_STATUS.CLEAN,
      category: SORT_STATUS.CLEAN,
      product: SORT_STATUS.CLEAN,
      quantity: SORT_STATUS.CLEAN,
      unitPrice: SORT_STATUS.CLEAN,
      totalPrice: SORT_STATUS.CLEAN,
      [`${value}`]:
        sort[`${value}`] == SORT_STATUS.ASC
          ? SORT_STATUS.DESC
          : SORT_STATUS.ASC,
    });
  };

  const _toggleFood = () => {
    setOpen(true);
  };

  const _editFood = (item: any) => {
    localStorage.setItem("food", JSON.stringify(item));
    _toggleFood();
  };

  const _deleteFood = (item: any) => {
    localStorage.setItem("deleteFood", JSON.stringify(item));
    setOpenDelete(true);
  };

  const _showIconSort = useCallback(
    (key: string) => {
      return (
        <span
          className={`ml-2 flex-none rounded bg-gray-100 text-gray-900 group-hover:bg-gray-200 cursor-pointer ${
            sort[`${key}`] == SORT_STATUS.ASC && "rotate-180"
          }`}
          onClick={() => _handleSort(key)}
        >
          {sort[`${key}`] == SORT_STATUS.CLEAN ? (
            <svg
              className="h-4 w-4"
              xmlns="http://www.w3.org/2000/svg"
              height="1em"
              viewBox="0 0 320 512"
            >
              <path d="M137.4 41.4c12.5-12.5 32.8-12.5 45.3 0l128 128c9.2 9.2 11.9 22.9 6.9 34.9s-16.6 19.8-29.6 19.8H32c-12.9 0-24.6-7.8-29.6-19.8s-2.2-25.7 6.9-34.9l128-128zm0 429.3l-128-128c-9.2-9.2-11.9-22.9-6.9-34.9s16.6-19.8 29.6-19.8H288c12.9 0 24.6 7.8 29.6 19.8s2.2 25.7-6.9 34.9l-128 128c-12.5 12.5-32.8 12.5-45.3 0z" />
            </svg>
          ) : (
            <svg
              className="h-4 w-4"
              xmlns="http://www.w3.org/2000/svg"
              height="1em"
              viewBox="0 0 320 512"
            >
              <path d="M182.6 470.6c-12.5 12.5-32.8 12.5-45.3 0l-128-128c-9.2-9.2-11.9-22.9-6.9-34.9s16.6-19.8 29.6-19.8H288c12.9 0 24.6 7.8 29.6 19.8s2.2 25.7-6.9 34.9l-128 128z" />
            </svg>
          )}
        </span>
      );
    },
    [sort],
  );

  return (
    <div className={classNameWrapper}>
      <div className="sm:flex sm:items-center">
        <div className="sm:flex-auto">
          <h1 className="text-base font-semibold leading-6 text-gray-900">
            {title}
          </h1>
        </div>
        <div className="mt-4 sm:ml-16 sm:mt-0 sm:flex-none">
          <button
            type="button"
            onClick={_toggleFood}
            className="block rounded-md bg-indigo-600 px-3 py-2 text-center text-sm font-semibold text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
          >
            {textButton}
          </button>
        </div>
      </div>
      <div className="mt-8 flow-root">
        <div className="-mx-4 -my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
          <div className="inline-block min-w-full py-2 align-middle sm:px-6 lg:px-8">
            <table className="min-w-full divide-y divide-gray-300">
              <thead>
                <tr>
                  <th
                    scope="col"
                    className="py-3.5 pl-4 pr-3 text-left text-sm font-semibold text-gray-900 sm:pl-0"
                  >
                    <div className="group inline-flex">
                      #
                      {/* <!-- Active: "bg-gray-200 text-gray-900 group-hover:bg-gray-300", Not Active: "invisible text-gray-400 group-hover:visible group-focus:visible" --> */}
                    </div>
                  </th>
                  <th
                    scope="col"
                    className="py-3.5 pl-4 pr-3 text-left text-sm font-semibold text-gray-900 sm:pl-0"
                  >
                    <div className="group inline-flex">
                      {i18n.t("food_sales.order_date")}
                      {/* <!-- Active: "bg-gray-200 text-gray-900 group-hover:bg-gray-300", Not Active: "invisible text-gray-400 group-hover:visible group-focus:visible" --> */}
                      {_showIconSort("createdon")}
                    </div>
                  </th>
                  <th
                    scope="col"
                    className="py-3.5 pl-4 pr-3 text-left text-sm font-semibold text-gray-900 sm:pl-0"
                  >
                    <div className="group inline-flex">
                      {i18n.t("food_sales.region")}
                      {/* <!-- Active: "bg-gray-200 text-gray-900 group-hover:bg-gray-300", Not Active: "invisible text-gray-400 group-hover:visible group-focus:visible" --> */}
                      {_showIconSort("region")}
                    </div>
                  </th>
                  <th
                    scope="col"
                    className="py-3.5 pl-4 pr-3 text-left text-sm font-semibold text-gray-900 sm:pl-0"
                  >
                    <div className="group inline-flex">
                      {i18n.t("food_sales.city")}
                      {/* <!-- Active: "bg-gray-200 text-gray-900 group-hover:bg-gray-300", Not Active: "invisible text-gray-400 group-hover:visible group-focus:visible" --> */}
                      {_showIconSort("city")}
                    </div>
                  </th>
                  <th
                    scope="col"
                    className="py-3.5 pl-4 pr-3 text-left text-sm font-semibold text-gray-900 sm:pl-0"
                  >
                    <div className="group inline-flex">
                      {i18n.t("food_sales.category")}
                      {/* <!-- Active: "bg-gray-200 text-gray-900 group-hover:bg-gray-300", Not Active: "invisible text-gray-400 group-hover:visible group-focus:visible" --> */}
                      {_showIconSort("category")}
                    </div>
                  </th>
                  <th
                    scope="col"
                    className="py-3.5 pl-4 pr-3 text-left text-sm font-semibold text-gray-900 sm:pl-0"
                  >
                    <div className="group inline-flex">
                      {i18n.t("food_sales.product")}
                      {/* <!-- Active: "bg-gray-200 text-gray-900 group-hover:bg-gray-300", Not Active: "invisible text-gray-400 group-hover:visible group-focus:visible" --> */}
                      {_showIconSort("product")}
                    </div>
                  </th>
                  <th
                    scope="col"
                    className="py-3.5 pl-4 pr-3 text-left text-sm font-semibold text-gray-900 sm:pl-0"
                  >
                    <div className="group inline-flex">
                      {i18n.t("food_sales.quantity")}
                      {/* <!-- Active: "bg-gray-200 text-gray-900 group-hover:bg-gray-300", Not Active: "invisible text-gray-400 group-hover:visible group-focus:visible" --> */}
                      {_showIconSort("quantity")}
                    </div>
                  </th>
                  <th
                    scope="col"
                    className="py-3.5 pl-4 pr-3 text-left text-sm font-semibold text-gray-900 sm:pl-0"
                  >
                    <div className="group inline-flex">
                      {i18n.t("food_sales.unit_price")}
                      {/* <!-- Active: "bg-gray-200 text-gray-900 group-hover:bg-gray-300", Not Active: "invisible text-gray-400 group-hover:visible group-focus:visible" --> */}
                      {_showIconSort("unitPrice")}
                    </div>
                  </th>
                  <th
                    scope="col"
                    className="py-3.5 pl-4 pr-3 text-left text-sm font-semibold text-gray-900 sm:pl-0"
                  >
                    <div className="group inline-flex">
                      {i18n.t("food_sales.total_price")}
                      {/* <!-- Active: "bg-gray-200 text-gray-900 group-hover:bg-gray-300", Not Active: "invisible text-gray-400 group-hover:visible group-focus:visible" --> */}
                      {_showIconSort("totalPrice")}
                    </div>
                  </th>
                  <th scope="col" className="relative py-3.5 pl-3 pr-0">
                    <span className="sr-only">{i18n.t("food_sales.edit")}</span>
                  </th>
                </tr>
              </thead>
              <tbody className="divide-y divide-gray-200 bg-white">
                {dataList?.map((item: any, index: number) => (
                  <tr key={JSON.stringify(item) + index}>
                    <td className="whitespace-nowrap py-4 pl-4 pr-3 text-sm text-gray-900 sm:pl-0">
                      {Number(indexItemLast) - (dataList?.length - 1 - index)}
                    </td>
                    <td className="whitespace-nowrap py-4 pl-4 pr-3 text-sm text-gray-900 sm:pl-0">
                      {moment(item?.createOn).format("DD/MM/YYYY")}
                    </td>
                    <td className="whitespace-nowrap py-4 pl-4 pr-3 text-sm text-gray-900 sm:pl-0">
                      {item?.region}
                    </td>
                    <td className="whitespace-nowrap py-4 pl-4 pr-3 text-sm text-gray-900 sm:pl-0">
                      {item?.city}
                    </td>
                    <td className="whitespace-nowrap py-4 pl-4 pr-3 text-sm text-gray-900 sm:pl-0">
                      {item?.category}
                    </td>
                    <td className="whitespace-nowrap py-4 pl-4 pr-3 text-sm text-gray-900 sm:pl-0">
                      {item?.product}
                    </td>
                    <td className="whitespace-nowrap py-4 pl-4 pr-3 text-sm text-gray-900 sm:pl-0">
                      {item?.quantity}
                    </td>
                    <td className="whitespace-nowrap py-4 pl-4 pr-3 text-sm text-gray-900 sm:pl-0">
                      {formatMoneyUSD(item?.unitPrice)}
                    </td>
                    <td className="whitespace-nowrap py-4 pl-4 pr-3 text-sm text-gray-900 sm:pl-0">
                      {formatMoneyUSD(item?.totalPrice)}
                    </td>
                    <td className="relative whitespace-nowrap py-4 pl-3 pr-4 text-right text-sm sm:pr-0">
                      <span
                        onClick={() => _editFood(item)}
                        className="text-indigo-600 hover:text-indigo-900 cursor-pointer"
                      >
                        {i18n.t("food_sales.edit")}
                        <span className="sr-only">
                          {i18n.t("food_sales.edit")}
                        </span>
                      </span>{" "}
                      | &nbsp;
                      <span
                        onClick={() => _deleteFood(item)}
                        className="text-red-600 hover:text-delete-900 cursor-pointer"
                      >
                        {i18n.t("food_sales.delete")}
                        <span className="sr-only">
                          {i18n.t("food_sales.delete")}
                        </span>
                      </span>
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
            {!isLoadingCallApi && dataList?.length == 0 && (
              <div className="text-center py-20 w-full">
                {i18n.t("food_sales.data_not")}
              </div>
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Table;
