import i18n from "@/app/i18n";
import { ChevronLeftIcon, ChevronRightIcon } from "@heroicons/react/20/solid";
import ReactPaginate from "react-paginate";

interface IPagination
  extends React.DetailedHTMLProps<
    React.HTMLAttributes<HTMLDivElement>,
    HTMLDivElement
  > {
  page: number;
  limit: number;
  totalData: number;
  setPage: any;
  innerRef?: any;
}

export default function Pagination({
  page,
  limit,
  totalData,
  setPage,
  innerRef,
  ...props
}: IPagination) {
  const from = Math.min((page - 1) * limit, totalData);
  const to = Math.min(page * limit, totalData);
  const listCountPage = [...Array(Math.ceil(totalData / limit))].map(
    (_x, i: number) => i + 1,
  );

  const _previous = () => {
    setPage(page - 1);
  };

  const _next = () => {
    setPage(page + 1);
  };

  const handlePageClick = ({ selected }: any) => {
    setPage(selected + 1);
  };

  return (
    <div
      {...props}
      className={`flex items-center justify-center md:justify-between bg-white py-3 ${props?.className}`}
    >
      <div
        className={`sm:flex-1 sm:justify-between flex flex-col md:flex-row justify-center items-center flex-wrap md:flex-nowrap gap-3`}
      >
        <div>
          <p
            className={`${
              totalData ? "block text-sm text-gray-700" : "hidden"
            }`}
          >
            <span className="font-medium">
              {totalData ? from + 1 : 0}~{to} / {`${totalData}`}
              {i18n.language == "en" ? " " : ""}
              {i18n.t("pagination.items")}
            </span>
          </p>
        </div>
        <div>
          <nav
            className={`${
              totalData ? "" : "hidden"
            } isolate inline-flex -space-x-px rounded-md shadow-sm`}
            aria-label="Pagination"
          >
            <ReactPaginate
              ref={innerRef}
              previousLabel={
                <button
                  onClick={_previous}
                  disabled={page == 1}
                  className={`relative inline-flex items-center rounded-l-md px-2 py-2 ring-[0.5px] ring-inset ring-gray-300 hover:bg-gray-50 focus:z-20 focus:outline-offset-0 ${
                    page == 1 && "cursor-not-allowed"
                  }`}
                >
                  <span className="sr-only">
                    {i18n.t("pagination.previous")}
                  </span>
                  <ChevronLeftIcon className="h-5 w-5" aria-hidden="true" />
                </button>
              }
              nextLabel={
                <button
                  onClick={_next}
                  disabled={page == listCountPage?.length}
                  className={`relative inline-flex items-center rounded-r-md px-2 py-2 ring-[0.5px] ring-inset ring-gray-300 hover:bg-gray-50 focus:z-20 focus:outline-offset-0 ${
                    page == listCountPage?.length && "cursor-not-allowed"
                  }`}
                >
                  <span className="sr-only">{i18n.t("pagination.next")}</span>
                  <ChevronRightIcon className="h-5 w-5" aria-hidden="true" />
                </button>
              }
              forcePage={page - 1}
              pageCount={listCountPage?.length}
              onPageChange={handlePageClick}
              containerClassName="flex"
              breakLinkClassName="relative inline-flex items-center px-4 py-2 text-sm font-semibold ring-[0.5px] ring-inset ring-gray-300 hover:bg-gray-50 focus:z-20 focus:outline-offset-0 cursor-pointer"
              pageLinkClassName="border-white relative inline-flex items-center px-4 py-2 text-sm font-semibold ring-[0.5px] ring-inset ring-gray-300 focus:z-20 focus:outline-offset-0 cursor-pointer"
              activeLinkClassName="text-white relative z-10 inline-flex items-center bg-indigo-700 hover:bg-indigo-500 px-4 py-2 text-sm font-semibold focus:z-20 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600 cursor-pointer"
              pageRangeDisplayed={2}
              marginPagesDisplayed={1}
            />
          </nav>
        </div>
      </div>
    </div>
  );
}
