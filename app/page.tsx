"use client";

import { useEffect, useState } from "react";
import moment from "moment-timezone";

import Pagination from "./components/pagination";
import Table from "./components/table";
import i18n from "./i18n";
import { SORT_STATUS } from "./config/constant";
import Input from "./components/input";
import Button from "./components/button";
import Modal from "./components/modal";
import Notification from "./components/notification";
import Food from "./components/food";
import { deleteFood, getListFood } from "./services/api/food.service";
import { useStoreActions, useStoreState } from "easy-peasy";
import { settingActionSelector, settingStateSelector } from "./store";

export default function Home() {
  const [isLoadingCallApi, setIsLoadingCallApi] = useState(true);
  const [page, setPage] = useState(1);
  const [limit] = useState(10);
  const [sort, setSort] = useState({
    createdon: SORT_STATUS.CLEAN,
    region: SORT_STATUS.CLEAN,
    city: SORT_STATUS.CLEAN,
    category: SORT_STATUS.CLEAN,
    product: SORT_STATUS.CLEAN,
    quantity: SORT_STATUS.CLEAN,
    unitPrice: SORT_STATUS.CLEAN,
    totalPrice: SORT_STATUS.CLEAN,
  });
  const [search, setSearch] = useState({
    toDate: "",
    fromDate: "",
    text: "",
  });
  const [dataList, setDataList] = useState<any[]>([]);
  const [totalDataList, setTotalDataList] = useState(0);
  const [open, setOpen] = useState(false);
  const [openAlert, setOpenAlert] = useState(false);

  const { setShowNotification, setStatusNotification, setMessageNotification } =
    useStoreActions(settingActionSelector);
  const { showNotification, statusNotification, messageNotification } =
    useStoreState(settingStateSelector);

  useEffect(() => {
    _getData();
  }, [page, limit, open, sort]);

  const _getData = async () => {
    try {
      setIsLoadingCallApi(true);
      const resData = await getListFood({
        ..._dataSort(),
        Pages: page,
        ItemPerPage: limit,
        SearchTerm: search?.text,
        OrderedFrom: search?.fromDate
          ? moment(search?.fromDate).format("YYYY-MM-DD")
          : "",
        OrderedTo: search?.toDate
          ? moment(search?.toDate).format("YYYY-MM-DD")
          : "",
      });

      if (resData) {
        setDataList(resData?.data?.orders);
        setTotalDataList(resData?.data?.totalItem);
      }
      setIsLoadingCallApi(false);
    } catch (error) {
      setIsLoadingCallApi(false);
    }
  };

  const _dataSort = () => {
    if (sort?.createdon !== SORT_STATUS.CLEAN) {
      return {
        SortByField: "CreatedOn",
        SortASC: sort?.createdon == SORT_STATUS.ASC ? true : false,
      };
    }

    if (sort?.region !== SORT_STATUS.CLEAN) {
      return {
        SortByField: "Region",
        SortASC: sort?.region == SORT_STATUS.ASC ? true : false,
      };
    }

    if (sort?.city !== SORT_STATUS.CLEAN) {
      return {
        SortByField: "City",
        SortASC: sort?.city == SORT_STATUS.ASC ? true : false,
      };
    }

    if (sort?.category !== SORT_STATUS.CLEAN) {
      return {
        SortByField: "Category",
        SortASC: sort?.category == SORT_STATUS.ASC ? true : false,
      };
    }

    if (sort?.product !== SORT_STATUS.CLEAN) {
      return {
        SortByField: "Product",
        SortASC: sort?.product == SORT_STATUS.ASC ? true : false,
      };
    }

    if (sort?.quantity !== SORT_STATUS.CLEAN) {
      return {
        SortByField: "Quantity",
        SortASC: sort?.quantity == SORT_STATUS.ASC ? true : false,
      };
    }

    if (sort?.unitPrice !== SORT_STATUS.CLEAN) {
      return {
        SortByField: "UnitPrice",
        SortASC: sort?.unitPrice == SORT_STATUS.ASC ? true : false,
      };
    }

    if (sort?.totalPrice !== SORT_STATUS.CLEAN) {
      return {
        SortByField: "TotalPrice",
        SortASC: sort?.totalPrice == SORT_STATUS.ASC ? true : false,
      };
    }

    return {
      SortByField: "CreatedOn",
      SortASC: false,
    };
  };

  const _search = () => {
    _getData();
  };

  const _changeValue = (e: any, key: string) => {
    setSearch({
      ...search,
      [`${key}`]: e?.target?.value,
    });
  };

  const _onCloseModal = async () => {
    setOpenAlert(false);
    localStorage.removeItem("deleteFood");
  };

  const handleOk = async () => {
    try {
      const deleteFoodStorage = JSON.parse(
        `${localStorage.getItem("deleteFood")}`,
      );

      if (deleteFoodStorage) {
        await deleteFood({
          id: deleteFoodStorage?.id,
        });
        await _getData();
        setShowNotification(true);
        setStatusNotification("success");
        setMessageNotification(`${i18n.t("food_sales.delete_food_success")}`);
      }

      _onCloseModal();
    } catch (error) {
      setIsLoadingCallApi(false);
      setShowNotification(true);
      setStatusNotification("error");
      setMessageNotification(`${i18n.t("food_sales.delete_food_error")}`);
    }
  };

  return (
    <main className="container mx-auto px-4 sm:px-6 lg:px-8 mt-3">
      <div className="grid text-center lg:grid-cols-4 lg:text-left">
        <Input
          label={`${i18n.t("food_sales.search_text")}`}
          id={"search_text"}
          placeholder={`${i18n.t("food_sales.search_text_placeholder")}`}
          classNameWrapper="mr-0 mb-3 lg:mr-3"
          value={search?.text}
          onChange={(e) => _changeValue(e, "text")}
          onKeyUp={(e) => {
            if (e.key == "Enter") {
              _getData();
            }
          }}
        />
        <Input
          label={`${i18n.t("food_sales.from_date")}`}
          id={"from_date"}
          placeholder={`${i18n.t("food_sales.from_date_placeholder")}`}
          type="date"
          classNameWrapper="mr-0 mb-3 lg:mr-3"
          value={search?.fromDate}
          max={moment(search?.toDate).format("YYYY-MM-DD")}
          onChange={(e) => _changeValue(e, "fromDate")}
        />
        <Input
          label={`${i18n.t("food_sales.to_date")}`}
          id={"to_date"}
          placeholder={`${i18n.t("food_sales.to_date_placeholder")}`}
          type="date"
          classNameWrapper="mr-0 mb-3 lg:mr-3"
          value={search?.toDate}
          min={moment(search?.fromDate).format("YYYY-MM-DD")}
          onChange={(e) => _changeValue(e, "toDate")}
        />

        <div className="flex items-end mb-3">
          <Button
            typeButton="primary"
            className="text-sm"
            style={{ paddingTop: "0.56rem", paddingBottom: "0.56rem" }}
            loading={false}
            onClick={_search}
          >
            {i18n.t("food_sales.search")}
          </Button>
        </div>
      </div>
      <Table
        title={`${i18n.t("food_sales.title")}`}
        textButton={`${i18n.t("food_sales.add_food_sale")}`}
        dataList={dataList}
        sort={sort}
        setSort={setSort}
        setOpen={setOpen}
        setOpenDelete={setOpenAlert}
        isLoadingCallApi={isLoadingCallApi}
        indexItemLast={page * limit}
      />
      <Pagination
        page={page}
        limit={limit}
        totalData={totalDataList}
        setPage={setPage}
      />
      <Modal
        open={openAlert}
        onClose={_onCloseModal}
        setOpen={setOpenAlert}
        title={`${i18n.t("food_sales.confirm_delete_food")}`}
        status="warning"
        handleOk={handleOk}
      />
      <Notification
        status={statusNotification}
        title={messageNotification}
        show={showNotification}
        setShow={setShowNotification}
      />
      {open && <Food open={open} setOpen={setOpen} />}
    </main>
  );
}
